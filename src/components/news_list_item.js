import React from "react";
import { css } from "glamor";

const NewsItem = ({ item }) => {
  let item_news = css({
    padding: "20px",
    boxSizing: "border-box",
    borderBottom: "1px solid grey",
    fontFamily: "Roboto"
  });

  let item_grey = css({
    background: "lightgrey"
  });

  return (
    <div {...item_news} {...item_grey}>
      <h3>{item.title}</h3>
      <div>{item.feed}</div>
    </div>
  );
};

export default NewsItem;
